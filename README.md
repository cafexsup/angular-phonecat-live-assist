# Angular Phone Catalog with CafeX Fusion Live Assist

This sample simply shows how to add Live Assist functionality to the angular.js 
sample application. Feel free to clone this app, but be sure to change the IP references to point to your own CafeX server.

You can read a more detailed article [here](https://support.cafex.com/hc/en-us/articles/202517061-Adding-Live-Assist-to-the-angularjs-PhoneCat-Tutorial-App).

